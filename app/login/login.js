//Declaration block
var frame = require("ui/frame");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var http = require("http");
var page;
var session_id;
var session_id2;
var appSettings = require("application-settings");
var dialogsModule = require("ui/dialogs");
var AccountList;
var pass;
var username;
var view = require("ui/core/view");
var appExit = require("nativescript-master-technology");
var pageData;
 
//binding the textfields from the xml with js. 		
  
//when the page loads, the session id is fetched and saved using the NxaSOAP and a given token.
function onNavigatingTo(args){
	page = args.object;
	page.bindingContext = pageData;
	page.bindingContext.set("showLogin",true);
	var encoded2 = encodeURI('https://web.vodcs.co.za:443/rest/Session/login/{}/{"login":"NxaSOAP","token":"7b2e4f0a-32b7-4094-a39c-9b0590409880"}');
	http.getJSON(encoded2).then (function (r){
		session_id = r.session_id;
		session_id2 = JSON.stringify(session_id);	
		appSettings.setString("session_id", JSON.stringify(session_id2))
	}, function(e){ 
		console.log(e);
	});  
};   
	pageData = new Observable({
		username: 'nxatelapp0791948508',
		password: 'ThaboMolamodi123',
	}); 
function login(args){
	var page =args.object;
	page.bindingContext.set("showLogin",false);
	//get the session id and save it.
	username = pageData.get("username");
	var enteredpassword = pageData.get("password");
 
	
	var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id": '+session_id2+'}/{"login": "'+username+'"}');
	http.getJSON(encoded).then(function (response) {
		if (response.faultstring){
			alert('Username Incorrect, Please verify or signup.');
			frame.topmost().navigate('login/login');
		}
		pass = response.customer_info.password;
		


        //checks if the password matches the one that the user entered
		if (pass == enteredpassword){
			appSettings.setString("customer_info", JSON.stringify(response.customer_info));
			pageData = new Observable({
				username: '',
		 		password: '',
				});  
			page.bindingContext.set("showLogin",true);
			frame.topmost().navigate("main/main");	
		}else if(pass != pageData.get("password")) {
			page.bindingContext.set("showLogin",true);
			dialogsModule.alert({
				title: "Login",
				message: "Password is Incorrect",
				okButtonText: "Try Again"
			}).then(function () {
			  console.log("Dialog closed!");
			});
		};
	},function (e) {
		console.log(e);
	 }); 
};
//navigates from one page to another
exports.signup = function() {
   frame.topmost().navigate("signup/signup");
};
exports.save = function() {
   frame.topmost().navigate("changeInfo/changeInfo");
};

exports.exit = function() {
	dialogsModule.confirm({
		title: "Login Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.forgot = function() {
	frame.topmost().navigate("forgotpass/forgotpass");
};

exports.onNavigatingTo = onNavigatingTo;
exports.login = login;