//Declaration block.
var frameModule = require("ui/frame");
var http = require("http");
var Observable = require("data/observable").Observable;
var observableArray = require("data/observable-array");
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var pageData = new observableModule.Observable();
var session_id;
var cust_info;
var dialogsModule = require("ui/dialogs");
var i_cust;
var drawer;
var status;
var cust_type;

exports.home =function(){
	frameModule.topmost().navigate("main/main");
};
exports.buy =function(){
	frameModule.topmost().navigate("buyVoucher/buyVoucher");
};  
function loaded(args){
	pageData.set("showLogin", false);
	var page = args.object;	
	session_id = JSON.parse(appSettings.getString("session_id"));
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	i_cust = cust_info.i_customer;
	cust_type = cust_info.i_customer_type;
	var curBal = parseFloat(cust_info.balance);
	curBal = curBal.toFixed(2);


	if(cust_type === "1" || cust_type === "3"){
 		alert('Access Denied! Only Resellers are allowed to View Distributors.');
 		frameModule.topmost().navigate("main/main");
 	};
	var custList = new observableArray.ObservableArray();	

	var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_list/{"session_id":'+session_id+'}/{"i_customer_type":"3","i_parent": "'+i_cust+'"}');
	 http.getJSON(encoded).then(function (r) {
	 	pageData.set("showLogin", true);
	 	 for (var i = 0; i<r.customer_list.length; i++) { 
	 	 	var curBal = parseFloat(r.customer_list[i].balance);
	     	curBal = curBal.toFixed(2);	 
		  	var cust = {
		  	 firstname: r.customer_list[i].firstname, 
		  	 balance: 'R'+curBal, 
		  	 phone: r.customer_list[i].phone1,
		  	 comp_name: r.customer_list[i].companyname
		  	  } 
		  	custList.push(cust); 
	  }}, function (e) {
	    console.log(e);
	});	

	drawer = page.getViewById("drawer");
		
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
    pageData.set("custList", custList);
	page.bindingContext = pageData;
};

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.sell = function(){
	frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
function onNavBtnTap() { 
   frameModule.topmost().navigate("main/main")
};
exports.recharge = function() {
  frameModule.topmost().navigate("recharge/recharge");
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};

exports.loaded = loaded;
exports.onNavBtnTap = onNavBtnTap;

