//Declaration Block,
var frameModule = require("ui/frame");
var drawer;
var Observable = require("data/observable").Observable;
var pageData = new Observable();
var webViewModule = require("ui/web-view");
var webView = new webViewModule.WebView();
var dialogsModule = require("ui/dialogs");

function pageLoaded(args) {

	page = args.object;
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
	pageData.set("url", "http://pay.nxatel.co.za/");
    drawer = page.getViewById("drawer");
	page.bindingContext = pageData;	

};

exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login") 
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
}
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
exports.invoice = function(){
	frameModule.topmost().navigate("invoice/invoice");
};
exports.sell = function() {
frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Buy Voucher Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.home = function(){
	frameModule.topmost().navigate("main/main");
};
exports.buy = function(){
	frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
exports.customer = function(){
	frameModule.topmost().navigate("customers/customers");
};
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.pageLoaded = pageLoaded;

