//Declaration block.
var frame = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var observable  = require("data/observable");
var page;
var appSettings = require("application-settings");
var i_cust;
var cust_info;
var comp_name;
var session_id;
var http = require("http");
var Observable = require("data/observable").Observable;
var pageData;
var dialogsModule = require("ui/dialogs");
var drawer; 
var ObservableArray = require("data/observable-array").ObservableArray;
//bind the label to the selected image path
function onNavigatingTo(args) {
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	session_id = JSON.parse(appSettings.getString("session_id"));
	var cust_type = cust_info.i_customer_type;
	
	if(cust_type === "1" || cust_type === "3"){
 		alert('Access Denied! Only Resellers and Sub-Customers are allowed to Add Distributors.');
 		frame.topmost().navigate("main/main");
 	};
	i_cust = cust_info.i_customer;
	comp_name= cust_info.companyname;
	pageData = new Observable({
		fname: '',
		lname: '',
		pass2: '',
		pass1: '',
		city: '',
		country:'',
		zip: '',
		phone: '',
		address:'',
		login:''
	});
	
	page = args.object;
	drawer = page.getViewById("drawer");
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
	page.bindingContext = pageData;
}
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.signedIn = function(){
	pageData.set("showLogin", false);
 	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();	
	if(dd<10) {
		dd='0'+dd
	} 
	if(mm<10) {
	    mm='0'+mm
	} 
	today = yyyy+'-'+mm+'-'+dd;

 	var fname = pageData.get("fname");
 	var lname = pageData.get("fname");
 	var add = pageData.get("address");
 	var city = pageData.get("city");
 	var zip = pageData.get("zip");
 	var country = pageData.get("country");
 	var phone = pageData.get("phone");
 	var email = pageData.get("email");
 	var login = pageData.get("login");
 	var pass1 = pageData.get("pass1");
 	var pass2 = pageData.get("pass2");
 	var companyname = pageData.get("comp_name");

	if (pageData.get("pass1") === pageData.get("pass2")){ 
		var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/add_customer/{"session_id":'+session_id+'}/{"customer_info": {"firstname":"'+fname+'","name":"'+companyname+'",  "lastname":"'+lname+'","i_customer_type":"1","i_parent":"'+i_cust+'","i_customer_class": "335","iso_4217":"ZAR","opening_balance":"0.00000","balance":"0.00000","companyname":"'+companyname+'","email":"'+email+'","phone1":"'+phone+'","baddr1":"'+add+'","city":"'+city+'","country":"'+country+'","note":"Distributor of a '+comp_name+'  Reseller","login":"'+login+'","password":"'+pass2+'","send_invoices":"Y","bill_status":"O"}}');
		http.getJSON(encoded).then(function (response) {
			pageData.set("showLogin", true);
			alert('Successfully Registered '+ fname+' as a Distributor.');
		}),function(e){
			pageData.set("showLogin", true);
			console.log('Error has Occured: ' + e);
		};
	}
	else{
		pageData.set("showLogin", true);
		dialogsModule.alert({
			title: "Sign Up",
			message: "Passwords dont match",
			okButtonText: "Try Again"
			}).then(function () {
			  console.log("Dialog closed!");
			});
	}
}

exports.exit = function() {
	dialogsModule.confirm({
		title: "Sign Up Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};

exports. account = function(){
	frame.topmost().navigate("account/account");
};
exports.customer = function(){
	frame.topmost().navigate("customers/customers");
};
exports.sell = function(){
	frame.topmost().navigate("sellVoucher/sellVoucher");
};
exports.buy = function(){
	frame.topmost().navigate("buyVoucher/buyVoucher");
};
function onNavBtnTap(){
	frame.topmost().navigate("main/main")
}
function home(){
	frame.topmost().navigate("main/main")
}
exports.logout = function(){
	frame.topmost().navigate("login/login")
}
exports.onNavigatingTo = onNavigatingTo;
exports.onNavBtnTap =onNavBtnTap;
exports.home =home;