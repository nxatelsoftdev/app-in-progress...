var http = require("http");
var cameraModule = require("camera");
var imageModule = require("ui/image");
var email = require("nativescript-email");
var view = require("ui/core/view");
var fs = require('file-system');
var enumsModule = require("ui/enums");
var imageContainer;

exports.loaded = function(args){
	 var page = args.object;
	 imageContainer = view.getViewById(page,"img");
};

exports.tapAction = function(){
	
	cameraModule.takePicture().then(function(picture)
	{
		imageContainer.imageSource = picture;
		
		var savepath = fs.knownfolders.documents().path;
		var filename = 'img_' + new Date().getTime() + '.jpg';
        var filepath = fs.path.join(savepath, filename);
		var picsaved = picture.saveToFile(filepath, enumsModule.ImageFormat.jpeg);
		
        if(picsaved) {
            console.log("Saving");
            var session = bghttp.session("image-upload");
            var request = {
                url: config.apiUrl,
                method: "POST",
                headers: {
                    "Content-Type": "application/octet-stream",
                    "File-Name": filename
                },
                description: "{ 'uploading': '" + filename + "' }"
            };

            var task = session.uploadFile("file://" + filepath, request);

            task.on("progress", logEvent);
            task.on("error", logEvent);
            task.on("complete", logEvent);
            function logEvent(e) {
                console.log(e.eventName);
            }
        } else {
            console.log("Failed To Save");
        }
    });

};


