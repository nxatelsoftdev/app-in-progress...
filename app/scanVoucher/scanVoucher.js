//Declaration block
var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var http = require("http");
var page;
var session_id;
var session_id2;
var appSettings = require("application-settings");
var dialogsModule = require("ui/dialogs");
var view = require("ui/core/view");
var amount;
var ZXing = require("nativescript-zxing");
var v_id;
var v_bal;
var bill_st;
var voucher;
var i_acc;
var cust_info;
var cust_type;
var i_cust;
var drawer;
var curBal;
var ImageModule = require("ui/image");
var cust_info = JSON.parse(appSettings.getString("customer_info"));

//var img = new ImageModule.Image();
//img.src = "http://spimeproject.com/wp-content/uploads/2010/07/spimebox_com_20.png";
var session_id = JSON.parse(appSettings.getString("session_id"));
//binds the xml textfields with the values on the js		
var pageData = new Observable({
	amount: '',
	voucherNumber: ''
});  
function loaded(args){
	pageData.set("showLogin", true);
	pageData.set("showDetails", true);
    var page = args.object;
	//Putting the blurry image, it gets the images from using the url and the image it does not contain any data.
	pageData.set ('image', "http://spimeproject.com/wp-content/uploads/2010/07/spimebox_com_20.png");
	voucher = page.navigationContext;
	v_id = voucher.id;
	v_bal = voucher.balance;
	console.dump(voucher);
	//storing the bill_staus of a selected voucher.
	bill_st = voucher.bill_status;
	i_acc = voucher.i_account;
	curBal = parseFloat(voucher.balance);
	curBal = curBal.toFixed(2);
	
	//letting a seller know that this voucher is not redeemed and button is written redeem.
	if (bill_st == "O"){
		pageData.set("showLogin", false);
		alert('Oops, This voucher has been sold, please choose another one.');
		frameModule.topmost().navigate("sellVoucher/sellVoucher");
	}else{
		pageData.set("showLogin", false);
		pageData.set('status',"Not redeemed");
		pageData.set('redeem',"Redee Now");
		pageData.set('done', "Go Back");	
		pageData.set("warning","Make sure you received money from the customer before you press REDEEM." );
	};
	
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
    drawer = page.getViewById("drawer");
	pageData.set('amount',"Amount: R "+curBal);
	pageData.set('voucherNumber',"Voucher No: #############");
	page.bindingContext = pageData;
}
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};

exports.home = function(){
	frameModule.topmost().navigate("main/main");
};
exports.customer = function(){
	frameModule.topmost().navigate("customers/customers");
};
exports.buy = function(){
	frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
exports.redeem = function(args){

	var i_cust = cust_info.i_customer;
	pageData.set("showLogin",true);
	pageData.set("showDetails", !pageData.get("showDetails"));
//	pageData.set("showDetails", false);

    var value = v_bal - (v_bal*0.1);

	dialogsModule.confirm("Do you want to send via SMS? ").then(function(resp){
		if(resp.result == true){
			dialogsModule.prompt("Enter Customer Number Below").then(function (r) {
			if(r.result == true){
				dialogsModule.prompt("Confirm Customer Number").then(function (res) {
					if(res.result == true){
						if(res.text == r.text){			
							//Info BIP API here...
							http.request({
								url: "https://api.infobip.com/sms/1/text/single",
								method: "POST",
								headers: {"Content-Type": "application/json","Authorization": "Basic TnhhdGVsVGVjOkd1aWRlb24yMDE2=="},
								content: JSON.stringify({from: "Nxatel Reseller App",to: r.text, text: "Thank you for recharging, your Voucher Number is: "+ v_id})
							}).then(function (response) {
								alert('Voucher Number: '+v_id+' sent to the Number: '+ r.text);	
							}, function (e) {
								console.log("Error occurred " + e);
							});			
						}else{
								alert('Customer Number Do not Match, Please Try Again');
						}
					}

				});

			}

		});
		};
		})

		pageData.set("showLogin", false);
		pageData.set('status',"Not Redeemed");
		pageData.set('redeem',"redeemed");

		var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Account/update_account/{"session_id": '+session_id+' }/{"account_info": {"i_account":"'+i_acc+'","bill_status":"O","inactive":"N"}}');
		//Getting the response from the above api, and deciding on any fields you want to work with.
		http.getJSON(encoded).then(function (response) {

		var username = cust_info.login;
		var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/make_transaction/{"session_id": '+session_id+'}/{"i_customer":"'+i_cust+'","action":"Manual payment","amount":"'+value+'"}');
		http.getJSON(encoded).then(function (r) {
			var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id": '+session_id+'}/{"login": "'+username+'"}');
			http.getJSON(encoded).then(function (response) {
				appSettings.setString("customer_info", JSON.stringify(response.customer_info));
			});
		},function (e) {
		   	console.log(e);
		});


			var zx = new ZXing();
			//get the customer infor and check the customer type of that particular customer.
			cust_info = JSON.parse(appSettings.getString("customer_info"));
			cust_type = cust_info.i_customer_type;
			i_cust = cust_info.i_customer;
			var qr = zx.createBarcode({
				encode: v_id, 
				height: 1000,
				width: 1000, 
				format: ZXing.QR_CODE
			});
				pageData.set("showLogin", false);
				pageData.set('status',"Redeemed");
				pageData.set('redeem',"Trt Again");
				pageData.set('done', "Go Back");
				pageData.set ('image', qr);
				pageData.set('voucherNumber',"Voucher No: "+voucher.id);
		}),function(e){
			console.log(e);
		};
	var page = args.object;
	page.bindingContext = pageData;



};
exports.done = function(){
	frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
function onNavBtnTap() {  
   frameModule.topmost().navigate("sellVoucher/sellVoucher")
};
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
function logout() {  
   frameModule.topmost().navigate("login/login")
};
exports.rechargeaccount = function(){
	frameModule.topmost().navigate("recharge/recharge");
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
}
exports.exit = function() {
	dialogsModule.confirm({
		title: "Scan Voucher Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.onNavBtnTap = onNavBtnTap;
exports.loaded =loaded;
exports.logout = logout;

