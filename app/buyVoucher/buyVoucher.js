//Declaration block
var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var appSettings = require("application-settings");
var session_id = JSON.parse(appSettings.getString("session_id"));
var cust_info = JSON.parse(appSettings.getString("customer_info"));
var i_cust = cust_info.i_customer;
var cust_bal = cust_info.balance;
var cust_type = cust_info.i_customer_type;
var http = require("http");
var view = require("ui/core/view");
var dialogsModule = require("ui/dialogs");
var page;
var drawer; 
var pageData;
var distr=[];
var observableArray = require("data/observable-array");

exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};  
//add values to a drop-down in the xml
function loaded(args){
    page = args.object;
 	pageData = new Observable({
		ten:  [0,10, 20, 30,40,50,60,70,80,90,100],
		tenSelected: 0,
		twenty: [0,10, 20, 30,40,50,60,70,80,90,100],
		twentySelected: 0,
		fifty: [0,10, 20, 30,40,50,60,70,80,90,100],
		fiftySelected: 0,
		hundred: [0,10, 20, 30,40,50,60,70,80,90,100],
		hundredSelected: 0,
		hundredFifty: [0,10, 20, 30,40,50,60,70,80,90,100],
		hundredFiftySelected:0,
		twohundred: [0,10, 20, 30,40,50,60,70,80,90,100],
		twohundredSelected: 0,
		threehundred: [0,10, 20, 30,40,50,60,70,80,90,100],
		threehundredSelected: 0,
	});
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
    drawer = page.getViewById("drawer");
	page.bindingContext = pageData;
	if(cust_type === "2" || cust_type === "3"){
 		alert('Access Denied! Only Retail Customers are allowed to Buy Vouchers.');
 		frameModule.topmost().navigate("main/main");
 	};
	pageData.set("showLogin", true);
};

//takes you the previous page
exports.logout = function() {
  frameModule.topmost().navigate("login/login") 
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.customer = function(){
	frameModule.topmost().navigate("customers/customers");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Buy Voucher Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		};
});
};
function onNavBtnTap() {  
   frameModule.topmost().navigate("main/main");
};
exports.home = function(){
	frameModule.topmost().navigate("main/main");
};
exports.account = function(){
	frameModule.topmost().navigate("account/account");
};
exports.sell = function(){
	frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
function buy(args){
	pageData.set("showLogin", false);
	var sum;
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
		
	if(dd<10) {
	    dd='0'+dd
	} 
	if(mm<10) {
	    mm='0'+mm
	} 
	today = yyyy+'-'+mm+'-'+dd;

	var tenindex = view.getViewById(page, "ten").selectedIndex;
	var twentyindex = view.getViewById(page, "twenty").selectedIndex;
	var fiftyvindex = view.getViewById(page, "fifty").selectedIndex;
	var hundredindex = view.getViewById(page, "hundred").selectedIndex;
	var hundredFiftyindex = view.getViewById(page, "hundredFifty").selectedIndex;
	var twoHundredindex = view.getViewById(page, "twohundred").selectedIndex;
	var threeHundredindex = view.getViewById(page, "threehundred").selectedIndex;

	//take the index and pass it to the array items to get the actual value
	var tenvalue = view.getViewById(page, "ten").items[tenindex];
	var twentyvalue = view.getViewById(page, "twenty").items[twentyindex];
	var fiftyvalue = view.getViewById(page, "fifty").items[fiftyvindex];
	var hundredvalue = view.getViewById(page, "hundred").items[hundredindex];
	var hundredFiftyvalue = view.getViewById(page, "twenty").items[hundredFiftyindex];
	var twohundredvalue = view.getViewById(page, "fifty").items[twoHundredindex];
	var threehundredvalue = view.getViewById(page, "hundred").items[threeHundredindex];


	cust_bal = parseFloat(cust_info.balance);
	cust_bal = cust_bal.toFixed(2);
 
	var tenPrice = tenvalue * 10;
	var twentyPrice = twentyvalue * 20;
	var fiftyPrice = fiftyvalue * 50;
	var hundredPrice = hundredvalue * 100;
	var hundredFiftyPrice = hundredFiftyvalue * 150;
	var twohundredPrice = twohundredvalue * 200;
	var threehundredPrice = threehundredvalue * 300;

	sum = tenPrice+twentyPrice+fiftyPrice+hundredPrice+hundredFiftyPrice+twohundredPrice+threehundredPrice;
 	var totVouchers = tenvalue + twentyvalue + fiftyvalue + hundredvalue + hundredFiftyvalue + twohundredvalue + threehundredvalue;
	var finalTotal = parseFloat(sum);
	finalTotal = finalTotal.toFixed(2);
	if(totVouchers<1){
		frameModule.topmost().navigate('buyVoucher/buyVoucher');
		alert('You did not select anything. Try Again.');
		pageData.set("showLogin",true);
	}else if(cust_bal>=sum){
		
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{"gen_method":"R", "gen_id_length":"13", "gen_amount":"'+tenvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"10.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"10.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));
		}),function(e){
			console.log(e);
		};
		
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{"gen_method":"R", "gen_id_length":"13", "gen_amount":"'+twentyvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"20.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"20.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));		
		}),function(e){
			console.log(e);
		};
	
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{"gen_method":"R", "gen_id_length":"13", "gen_amount":"'+fiftyvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"50.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"50.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));		
		}),function(e){
			console.log(e);
		};
			
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{"gen_method":"R", "gen_id_length":"13", "gen_amount":"'+hundredvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"100.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"100.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));		
		}),function(e){
			console.log(e);
		};
			
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{ "gen_method":"R", "gen_id_length":"13", "gen_amount":"'+hundredFiftyvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"150.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"150.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));		
		}),function(e){
			console.log(e);
		};
			
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{ "gen_method":"R", "gen_id_length":"13", "gen_amount":"'+twohundredvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"200.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"200.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));		
		}),function(e){
			console.log(e);
		};
			
			
		var encoded=encodeURI('https://web.vodcs.co.za:443/rest/Account/generate_accounts/{"session_id":'+session_id+'}/{ "gen_method":"R", "gen_id_length":"13", "gen_amount":"'+threehundredvalue+'","batch":"'+today+'","inactive":"Y", "blocked":"N","billing_model":"0","i_product":"1184","iso_639_1":"en","iso_4217":"ZAR","opening_balance":"300.00000","i_customer":"'+i_cust+'","bill_status":"I","activation_date":"'+today+'","balance":"300.00000"}');
		http.getJSON(encoded).then(function (response){
			console.log(JSON.stringify(response));		
		}),function(e){
			console.log(e);
		};
			

		var username = cust_info.login;
		var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/make_transaction/{"session_id": '+session_id+'}/{"i_customer":"'+i_cust+'","action":"Manual payment","amount":"'+finalTotal+'"}');
		http.getJSON(encoded).then(function (r) {
			var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id": '+session_id+'}/{"login": "'+username+'"}');
			http.getJSON(encoded).then(function (response) {
				pageData.set("showLogin",true);
				appSettings.setString("customer_info", JSON.stringify(response.customer_info));
				alert('Succesfully Bought ' +totVouchers+' Vouchers');
			});
		},function (e) {
		   	console.log(e);
		});
	}else{
		pageData.set("showLogin",true);
		alert('Insufficient fund, your current balance is R' + cust_bal +' and vouchers total is R'+finalTotal);
	};
};
exports.onNavBtnTap = onNavBtnTap;
exports.loaded = loaded;
exports.buy = buy;