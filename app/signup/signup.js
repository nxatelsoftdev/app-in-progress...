//Declaration block.
var frame = require("ui/frame");
var cameraModule = require("camera");
var myImage;
var pic;
var imageModule = require("ui/image");
var dialogsModule = require("ui/dialogs");
var view = require("ui/core/view");
var emailll = require("nativescript-email");
var http = require("http");
var appSettings = require("application-settings");
var session_id = JSON.parse(appSettings.getString("session_id"));
var page;
var pageData;
var imageSource = require('image-source');
var fs = require("file-system");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var list;
var filepath;

//bind the label to the selected image path
function onNavigatingTo(args) {
	emailll.available().then(function(avail) {
      console.log("Email available? " + avail);
	  if(avail != true){
		  alert('Your phone does not have an email. Please use other phone or create one.');
	  }
  	})
	page = args.object;
 	myImage = page.getViewById("thumb");
  	list = page.getViewById("urls-list");

	pageData = new Observable({
		fname: '', 
		lname: '',
		email: '',  
		tell: '', 
		pass1: '',
		pass2: '' 
	});
	page.bindingContext = pageData;
};	

exports.capture = function(args){
	 var imgPicker = require("nativescript-imagepicker");
	 var context = imgPicker.create({
         mode: "single"
      });

	  startSelection(context);
};
function startSelection(context) {
    context
        .authorize()
        .then(function() {
            list.items = [];
            return context.present();
        })
        .then(function(selection) {
            selection.forEach(function(selected) {
				//The fileUri doesnt wanna work, but the imagepicker works.
				
               /* alert("uri: " + selected.uri);           
                alert("fileUri: " + selected.fileUri);*/
				filepath = selected.uri;
            });
            list.items = selection;
        }).catch(function (e) {
            console.log(e);
        });
}
exports.register = function(){
	if (pageData.get("pass1") === pageData.get("pass2")){ 	
		var pass = pageData.get('pass2');
		var phone = pageData.get('tell');
		var email1 =pageData.get('email');
		var fname = pageData.get('fname');
		var lname = pageData.get('lname');
		var add = pageData.get('address');
		var faxn = pageData.get('fax');
		var username=pageData.get('username');
		var country = pageData.get('country');
		var add2 = pageData.get('address2');
		var province = pageData.get('province');
		var city = pageData.get('city');
		var companyname = pageData.get('companyname');
		//var selfie = pageData.get('thumb');
 
  		emailll.compose({
			subject: "Reseller App New Recruit",
			body: "<strong>First-Name: </strong>"+ fname +"		"+" <br />" + 
			"<strong>Last-Name: </strong>"+ lname + "		"+"<br />"+
			"<strong>Email: </strong>"+ email1 + "		"+"<br />" +
			"<strong>Username: </strong>"+ username +"		"+ "<br />"+ 
			"<strong>Password: </strong>"+ pass +"		"+ "<br />"+
			"<strong>Phone: </strong>"+ phone + "		"+"<br />" + 
			"<strong>Address: </strong>"+ add + "		"+"<br />"+
			"<strong>Address 2: </strong>"+ add2 + "		"+"<br />"+
			"<strong>Fax: </strong>"+ faxn + "		"+"<br />"+
			"<strong>Country: </strong>"+ country + "		"+"<br />"+
			"<strong>Province: </strong>"+ province + "		"+"<br />"+
			"<strong>City: </strong>"+ city + "		"+"<br />"+
			"<strong>Company Name: </strong>"+ companyname +"		"+"<br />" ,	
			to: ['guideon.katumbi@nxatel.co.za'],
			//cc: ['support@nxatel.co.za'],    
			attachments: [{
				 fileName: 'profile_pic.jpg',
				 path: filepath,
				 mimeType: 'jpeg/jpg'
			}],
				appPickerTitle: 'Complete with..' // for Android, default: 'Open with..' 
			}).then(function() {
				console.log("Email composer closed");
			});

	}else{
		dialogsModule.alert({
			title: "Sign Up",
			message: "Passwords dont match",
			okButtonText: "Try Again"
			}).then(function () {
			  console.log("Dialog closed!");
			});
	};
};

exports.exit = function() {
	dialogsModule.confirm({
		title: "Sign Up Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};

function onNavBtnTap(){
	frame.topmost().navigate("login/login");
};
exports.login = function(){
	frame.topmost().navigate("login/login");
};
exports.onNavigatingTo = onNavigatingTo;
exports.onNavBtnTap =onNavBtnTap;