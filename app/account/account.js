//Declaration block.
var frameModule = require("ui/frame");
var http = require("http");
var Observable = require("data/observable").Observable;
var observableArray = require("data/observable-array");
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var pageData = new observableModule.Observable();
var session_id;
var cust_info;
var dialogsModule = require("ui/dialogs");
var i_cust;
var drawer;
var status;
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.home =function(){
	frameModule.topmost().navigate("main/main");
}
function loaded(args){
	var page = args.object;
	drawer = page.getViewById("drawer");
	session_id = JSON.parse(appSettings.getString("session_id"));
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	var name = cust_info.firstname;
	var lname = cust_info.lastname;
	//var cust_id = cust_info.i_customer;
	i_cust = cust_info.i_customer;
	var tel = cust_info.phone1;
	var add = cust_info.baddr1;
	var bal = parseFloat(cust_info.balance);
	var email = cust_info.email;
	var lname = cust_info.lastname;
	var fax = cust_info.faxnum;
	var username = cust_info.login;
	var compa_name = cust_info.companyname;
	var cust_type = cust_info.i_customer_type;
	var add2 = cust_info.baddr2;
	var country = cust_info.country;
	var province = cust_info.state;
	var refnum = cust_info.refnum;
	var city = cust_info.city;
	if(cust_type == 1){
		status = "Retail/Sub-Customer";
	}else if(cust_type == 2){
		status="Reseller";
	}else if(cust_type == 3){
		status ="Distributor";
	}


	 //using the session_id2 with the users login details in order to access their customer information.
		
		// it links an xml "AccList" variable to a js AccList variable
				var menu = 0xe9ba;      
        pageData.set("menu", String.fromCharCode(menu));
        var acc = 0xeaeb;      
        pageData.set("acc", String.fromCharCode(acc));

        pageData.set("cust_type", status);
				pageData.set("name", name);
				pageData.set("sur" , lname);
				//pageData.set("CustNo", cust_id);
				pageData.set("address", add);
				pageData.set("tel", tel);
				pageData.set("email", email);
				bal = bal.toFixed(2);
				pageData.set("balance", bal);
				pageData.set("username" , username);
				pageData.set("fax", fax);
				pageData.set("comp_name", compa_name);
				pageData.set("country", country);
				pageData.set("province", province);
				pageData.set("city", city);
				pageData.set("address2" , add2);
				pageData.set("ref_num", refnum);


		page.bindingContext = pageData;
}
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.invoice = function() {
  frameModule.topmost().navigate("invoice/invoice");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
}
exports.transfer = function(){
	frameModule.topmost().navigate("transfer/transfer");
}
function onNavBtnTap() { 
   frameModule.topmost().navigate("main/main")
}
exports.recharge = function() {
  frameModule.topmost().navigate("recharge/recharge");
};
exports.customer = function() {
  frameModule.topmost().navigate("customers/customers");
};
exports.buy = function() {
  frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
exports.sell = function() {
  frameModule.topmost().navigate("sellVoucher/sellVoucher");
};

exports.loaded = loaded;
exports.onNavBtnTap = onNavBtnTap;

