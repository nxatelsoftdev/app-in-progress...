//Declaration Block.
var session_id;
var session_id2;
var http = require("http");
var Observable = require("data/observable").Observable;
var observableArray = require("data/observable-array");
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var frameModule = require("ui/frame");
var page;
var dialogsModule = require("ui/dialogs");
var drawer;
var pageData = new Observable({});

exports.loaded = function(args){
	pageData.set("username", '');
	pageData.set("companyname", '');
	pageData.set("email", '');
	page = args.object;
	drawer = page.getViewById("drawer");
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
	page.bindingContext=pageData;
};
exports.getpass = function() {
	pageData.set("showLogin", false);
	var session_id = JSON.parse(appSettings.getString("session_id"));
	var login = pageData.get('username');
	var companyname = pageData.get('compname');
	var email =pageData.get('email');

	var encoded = encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id":'+session_id+'}/{"login": "'+login+'","companyname":"'+companyname+'","email":"'+email+'"}');

	http.getJSON(encoded).then(function (response) {
		pageData.set("showLogin", true);
		dialogsModule.alert({
			title: "Password",
			message: "Your Password Is : "+response.customer_info.password,
			okButtonText: "Close"
			}).then(function () {
			  console.log("Dialog closed!");
			});
		frameModule.topmost().navigate("login/login");
		}),function(e){
		console.log(e);
	}
};
exports.login = function() {
  frameModule.topmost().navigate("login/login");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Forget Password Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};

//takes you to a previous page
function onNavBtnTap() {  
   frameModule.topmost().navigate("login/login")
};
exports.onNavBtnTap = onNavBtnTap;
