//Declaration are all made here.
var http = require("http");
var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var session_id;
var session_id2;
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array");
var appSettings = require("application-settings");
var AccountList = new ObservableArray.ObservableArray([]);
var cust_info;
var	pageData;
var drawer;
//when you press a buy button this will take you to a folder named buyVoucher and a file named BuyVoucher.
exports.buy = function() {
   frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
//when you press a rechargeaccount button this will take you to a folder named recharge and a file named recharge.
exports.rechargeaccount = function() {
   frameModule.topmost().navigate("recharge/recharge");
};
//when you press a sell button this will take you to a folder named sellVoucher and a file named sellVoucher.
exports.sell = function() {
frameModule.topmost().navigate("sellVoucher/sellVoucher");
}; 
//when you press a buy change this will take you to a folder named changeInfo and a file named changeInfo.
exports.change = function() {
  frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.main = function() {
  frameModule.topmost().navigate("main/main");
};
//when you press a back button on the action bar on top of the interface, this will take you the previous page.
function onNavBtnTap() {
   
   frameModule.topmost().navigate("main/main")
}
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
function onNavigatingTo(args){
  	var page = args.object;
	var cust_type;
	drawer = page.getViewById("drawer");
	//when the page load for the first time, the binding is done and the information on the main page are populated according to the user who logged in.
	
	//retreive the customer information from the stored variable.
	cust_info = JSON.parse(appSettings.getString("customer_info"));

/*
	var encoded = encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_my_info/{"session_id":'+session_id+'}/{}');

	http.getJSON(encoded).then(function (response) {
		dialogsModule.alert({
			title: "Password",
			message: "Your Password Is : "+response.customer_info.password,
			okButtonText: "Close"
			}).then(function () {
			  console.log("Dialog closed!");
			});
		frameModule.topmost().navigate("login/login");
		}),function(e){
		console.log(e);
	}

*/
	pageData = new Observable({
		comp_name: cust_info.companyname
	
	});
	
	var menu = 0xe9ba;      
        pageData.set("menu", String.fromCharCode(menu));
    var acc = 0xe976;      
        pageData.set("iconAcc", String.fromCharCode(acc));
 	var vou = 0xe938;      
        pageData.set("iconVou", String.fromCharCode(vou));
   var top = 0xe93d;      
        pageData.set("iconTop", String.fromCharCode(top)); 
    var rec = 0xe93a;      
        pageData.set("iconRec", String.fromCharCode(rec));  
    var cust = 0xe972;      
        pageData.set("iconCust", String.fromCharCode(cust));   
    var trans = 0xe901;      
        pageData.set("iconTrans", String.fromCharCode(trans));  
    var rew = 0xe99e;      
        pageData.set("iconRew", String.fromCharCode(rew));    
	page.bindingContext = pageData;
}

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.onNavBtnTap = onNavBtnTap;
exports.onNavigatingTo = onNavigatingTo;
