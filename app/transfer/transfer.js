//Declaration are all made here.
var http = require("http");
var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array");
var appSettings = require("application-settings");
var AccountList = new ObservableArray.ObservableArray([]);
var	cust_info = JSON.parse(appSettings.getString("customer_info"));
var	pageData; 
var drawer;
var after_buy_bal;
var session_id = JSON.parse(appSettings.getString("session_id"));
//when you press a buy button this will take you to a folder named buyVoucher and a file named BuyVoucher.
exports.buy = function() {
   frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
//when you press a rechargeaccount button this will take you to a folder named recharge and a file named recharge.
exports.rechargeaccount = function() {
   frameModule.topmost().navigate("recharge/recharge");
};
exports.customers = function() {
   frameModule.topmost().navigate("customers/customers");
}; 
//when you press a sell button this will take you to a folder named sellVoucher and a file named sellVoucher.
exports.sell = function() {
frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
//when you press a buy change this will take you to a folder named changeInfo and a file named changeInfo.
exports.change = function() {
  frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.trans = function() {
  frameModule.topmost().navigate("trans/trans");
};
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
//when you press a back button on the action bar on top of the interface, this will take you the previous page.
function onNavBtnTap() {
   
   frameModule.topmost().navigate("login/login")
};
               
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.invoice = function(){
	frameModule.topmost().navigate("invoice/invoice");
};
function onNavigatingTo(args){
  	var page = args.object;
    pageData = new Observable({
			number: '',
			amount: ''
  	});
     
		drawer = page.getViewById("drawer");
    pageData.set("showLogin", true);
		//when the page load for the first time, the binding is done and the information on the main page are populated according to the user who logged in.
	
		//retreive the customer information from the stored variable.
		cust_info = JSON.parse(appSettings.getString("customer_info"));
    //after_buy_bal = JSON.parse(appSettings.getString("balance"));
    var curBal = parseFloat(cust_info.balance);
		curBal = curBal.toFixed(2);
    pageData.set('balance', curBal);
    pageData.set('comp_name', cust_info.companyname);

    var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
    page.bindingContext = pageData;
};

function transfer(args){

    var page =args.object;
    page.bindingContext.set("showLogin",false);
    //get the session id and save it.
    var ent_num = pageData.get("number");
    var ent_amt = pageData.get("amount");
    pageData.set("showLogin", false);
    var i_cust = cust_info.i_customer;
    var rec_i_customer;

    //trim the balance from PortaOne.
     var curBal = parseFloat(cust_info.balance);
	   curBal = curBal.toFixed(0);

      var amt = parseFloat(ent_amt);
	    amt = amt.toFixed(0);
  //check the senders balance if is enough

    if(Number(curBal)>Number(amt)){

      //get customer info using the refnum. this will also check if the user exist
      var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id": '+session_id+'}/{"refnum": "'+ent_num+'"}');
        http.getJSON(encoded).then(function (respons) {
          //check if the user exist
          if(respons.faultstring){ 
            alert('Recipient does not exist.');
            frameModule.topmost().navigate("transfer/transfer");
          }else{
             
            //if the user exist, then trans cash from one user to another
              rec_i_customer = respons.customer_info.i_customer;
              //duduct from the sender
              var encoded2= encodeURI('https://web.vodcs.co.za:443/rest/Customer/make_transaction/{"session_id": '+session_id+'}/{"i_customer":"'+i_cust+'","action":"Manual payment","amount":"'+Number(ent_amt)+'"}');
              http.getJSON(encoded2).then(function (resp) {
            
               //now resave customer information.
                var encoded3= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id": '+session_id+'}/{"i_customer": "'+i_cust+'"}');
                http.getJSON(encoded3).then(function (r) {
                  appSettings.setString("customer_info", JSON.stringify(r.customer_info));
                }),function(e){
                  console.log('Error Occured ' + e);
                };
            
                //add to the receiver.
                var encoded3= encodeURI('https://web.vodcs.co.za:443/rest/Customer/make_transaction/{"session_id": '+session_id+'}/{"i_customer":"'+rec_i_customer+'","action":"Refund","amount":"'+Number(ent_amt)+'"}');
                http.getJSON(encoded3).then(function (respo) {
                  pageData.set("showLogin", true);
                }),function(e){
                  console.log('Error Occured ' + e);
                };
                //everything is well, you can not display a success message.
               
                alert('Successfully Topped up '+ent_num+' with the amount of R'+ent_amt);
                 frameModule.topmost().navigate("account/account");
            }),function(e){
                console.log('Error Occured ' + e);
              };   
          };

      }),function(e){
        console.log('Error Occured ' + e);
      };

    }else{
      //user do not have enough cash
      alert('You do not have enough balance to make that transaction.');
    };

};

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.onNavBtnTap = onNavBtnTap;
exports.onNavigatingTo = onNavigatingTo;
exports.transfer = transfer;