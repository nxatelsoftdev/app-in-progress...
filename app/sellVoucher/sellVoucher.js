//Declaration block.
var frameModule = require("ui/frame");
var http = require("http");
var Observable = require("data/observable").Observable;
var observableArray = require("data/observable-array");
var AccList10;
var AccList20;
var AccList50;
var AccList100;
var Account150;
var Account200;
var Account300;
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var pageData = new observableModule.Observable();
var session_id;
var cust_info;
var i_cust;
var used;
var dialogsModule = require("ui/dialogs");
var final_qr;
var drawer;
exports.home =function(){
	frameModule.topmost().navigate("main/main");
};
function loaded(args){

	pageData.set("showLogin", false);
	var page = args.object;
	drawer = page.getViewById("drawer");
	session_id = JSON.parse(appSettings.getString("session_id"));
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	i_cust = cust_info.i_customer;
	 //using the session_id2 with the users login details in order to access their customer information.
	var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Account/get_account_list/{"session_id": '+session_id+'}/{"limit":"1000","get_total":"1","i_customer": '+i_cust+'}');
	//Getting the response from the above api, and deciding on any fields you want to work with.
	http.getJSON(encoded).then(function (response) {

		//for loop to display the response of the api with the fields from the xml(interface).
		var filteredAccounts150 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 150.00;
		});

		var filteredAccounts10 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 10.00;	
		});
		 
		var filteredAccounts20 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 20.00;
		});
		
		var filteredAccounts50 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 50.00;
		});
		var filteredAccounts100 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 100.00;
		});

		var filteredAccounts200 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 200.00;
		});

		var filteredAccounts250 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 250.00;
		});

		var filteredAccounts300 = response.account_list.filter(function(account){
			return account.bill_status == 'I' && account.balance == 300.00;
		});

		/*var st = response.account_list.filter(function(account ){

			return parseFloat(account.bill_status); 
		});*/

		AccList10 = new observableArray.ObservableArray(filteredAccounts10);
		AccList20 = new observableArray.ObservableArray(filteredAccounts20);
		AccList50 = new observableArray.ObservableArray(filteredAccounts50);
		AccList100 = new observableArray.ObservableArray(filteredAccounts100);
		AccList150 = new observableArray.ObservableArray(filteredAccounts150);
		AccList200 = new observableArray.ObservableArray(filteredAccounts200);
		AccList300 = new observableArray.ObservableArray(filteredAccounts300);

		pageData.set("AccList10", AccList10);
		pageData.set("AccList20", AccList20);
		pageData.set("AccList50", AccList50);
		pageData.set("AccList100", AccList100);
		pageData.set("AccList150", AccList150);
		pageData.set("AccList200", AccList200);
		pageData.set("AccList300", AccList300);
		pageData.set("count",'Total Vouchers Bought By Far: ' + response.total);
		pageData.set("showLogin", true);
	}, function (e) {
	//catching any error that has occurred during the getJSON call.
		 console.log(e);
		});	

		pageData.itemTapped10 = itemTapped10;	
		pageData.itemTapped20 = itemTapped20;	
		pageData.itemTapped50 = itemTapped50;
		pageData.itemTapped100 = itemTapped100;	
		pageData.itemTapped150 = itemTapped150;	
		pageData.itemTapped200 = itemTapped200;
		pageData.itemTapped300 = itemTapped300;	
		// it links an xml "AccList" variable to a js AccList variable
		var menu = 0xe9ba;      
        pageData.set("menu", String.fromCharCode(menu));
		//pageData.set('status', "Used");
		page.bindingContext = pageData;

};
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.rechargeaccount = function(){
	frameModule.topmost().navigate("recharge/recharge");
}
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.buy = function(){
	frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
exports.customer = function(){
	frameModule.topmost().navigate("customers/customers");
};
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
exports.sell = function() {
  frameModule.topmost().navigate("sellVoucher/sellVoucher");
};

exports.addcust = function() {
  frameModule.topmost().navigate("addcust/addcust");
};

exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
function listViewItemTap(args){
	frameModule.topmost().navigate("scanVoucher/scanVoucher");
};
function onNavBtnTap() { 
   frameModule.topmost().navigate("main/main");
};

function itemTapped150(args){
	var voucher150 = pageData.get("AccList150").getItem(args.index);

	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher150,				
	});
};

function itemTapped200(args){
	var voucher200 = pageData.get("AccList200").getItem(args.index);

	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher200,				
	});
};

function itemTapped300(args){
	var voucher150 = pageData.get("AccList150").getItem(args.index);

	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher300,				
	});
};

function itemTapped10(args){
	var voucher10 = pageData.get("AccList10").getItem(args.index);
	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher10,				
	});
};

function itemTapped20(args){
	var voucher20 = pageData.get("AccList20").getItem(args.index);

	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher20,
				
	});
};

function itemTapped50(args){
	var voucher50 = pageData.get("AccList50").getItem(args.index);

	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher50,
				
	});
};

function itemTapped100(args){
	var voucher100 = pageData.get("AccList100").getItem(args.index);

	frameModule.topmost().navigate({
		moduleName: "scanVoucher/scanVoucher",
			context:
			    voucher100,
		
					
	});
};

exports.loaded = loaded;
exports.listViewItemTap = listViewItemTap;
exports.onNavBtnTap = onNavBtnTap;

