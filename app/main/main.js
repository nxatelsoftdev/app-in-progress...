//Declaration are all made here.
var http = require("http");
var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var session_id;
var session_id2;
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array");
var appSettings = require("application-settings");
var AccountList = new ObservableArray.ObservableArray([]);
var cust_info;
var	pageData;
var drawer;
var after_buy_bal;
//when you press a buy button this will take you to a folder named buyVoucher and a file named BuyVoucher.
exports.buy = function() {
   frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
//when you press a rechargeaccount button this will take you to a folder named recharge and a file named recharge.
exports.rechargeaccount = function() {
   frameModule.topmost().navigate("recharge/recharge");
};
exports.customers = function() {
   frameModule.topmost().navigate("customers/customers");
};
//when you press a sell button this will take you to a folder named sellVoucher and a file named sellVoucher.
exports.sell = function() {
frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
//when you press a buy change this will take you to a folder named changeInfo and a file named changeInfo.
exports.change = function() {
  frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.trans = function() {
  frameModule.topmost().navigate("trans/trans");
};
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
//when you press a back button on the action bar on top of the interface, this will take you the previous page.
function onNavBtnTap() {
   
   frameModule.topmost().navigate("login/login")
}
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
}; 
exports.invoice = function(){
	frameModule.topmost().navigate("invoice/invoice");
};

exports.support = function(args){
  	var page = args.object;
    var phone = require( "nativescript-phone" );
    phone.dial("+2786-169-2835",true);
};
function onNavigatingTo(args){
  	var page = args.object;
		var cust_type;
		drawer = page.getViewById("drawer");
		//when the page load for the first time, the binding is done and the information on the main page are populated according to the user who logged in.
	
		//retreive the customer information from the stored variable.
		cust_info = JSON.parse(appSettings.getString("customer_info"));
    //after_buy_bal = JSON.parse(appSettings.getString("balance"));

		var curBal = parseFloat(cust_info.balance);
		curBal = curBal.toFixed(2);

		pageData = new Observable({
			balance: 'R ' + curBal,
			comp_name: cust_info.companyname
	}); 
	  
	var menu = 0xe9ba;      
  pageData.set("menu", String.fromCharCode(menu));
	page.bindingContext = pageData;
};

exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.onNavBtnTap = onNavBtnTap;
exports.onNavigatingTo = onNavigatingTo;
