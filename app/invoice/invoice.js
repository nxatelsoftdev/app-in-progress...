//Declaration block.
var frameModule = require("ui/frame");
var http = require("http");
var Observable = require("data/observable").Observable;
var observableArray = require("data/observable-array");
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var pageData = new observableModule.Observable();
var datePicker = require("ui/date-picker");
var session_id;
var cust_info;
var dialogsModule = require("ui/dialogs");
var i_cust;
var drawer;
var status;
var page;
var maxD;
var InvList;
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();	
if(dd<10) {
    dd='0'+dd;
} ;
if(mm<10) {
    mm='0'+mm;
};

exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.home =function(){
	frameModule.topmost().navigate("main/main");
};
function loaded(args){

	page = args.object;
	drawer = page.getViewById("drawer");
	session_id = JSON.parse(appSettings.getString("session_id"));
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	i_cust = cust_info.i_customer;
	var y = datePicker.year = yyyy;
	var m = datePicker.month = mm;
	var d = datePicker.day = dd;
	var minD = datePicker.minDate = new Date(2010,0,01);
	maxD = datePicker.maxDate = new Date(yyyy, mm-1, dd);
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));

	pageData.set('day', d);
	pageData.set('month' , m);
	pageData.set('year', y);
	pageData.set('minD', minD);
	pageData.set('maxD', maxD);
	page.bindingContext = pageData;
	pageData.set("showLogin", true);
};
exports.get_date = function() {
	pageData.set("showLogin", false);
	var day = page.getViewById("date").day.toString();
    var mon = page.getViewById("date").month.toString();
    var year = page.getViewById("date").year.toString();
    if(day<10) {
	    day='0'+day;
	} ;
	if(mon<10) {
	    mon='0'+mon;
	};
    var from_date = year+'-'+mon+'-'+day;
    today= yyyy+'-'+mm+'-'+dd;


    InvList = new observableArray.ObservableArray();
	var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_invoices/{"session_id":'+session_id+'}/{"i_customer": "'+i_cust+'","from_date":"'+from_date+'","to_date":"'+today+'"}');	 
	http.getJSON(encoded).then(function (r) {
		pageData.set("showLogin", true);
		pageData.set('total', 'Total Invoices: ' + r.total);
	     for (var i = 0; i<r.total; i++){ 

	     	var amt_paid = parseFloat(r.invoices[i].amount_paid);
			amt_paid = amt_paid.toFixed(2);

			var amt_due = parseFloat(r.invoices[i].amount_due);
			amt_due = amt_due.toFixed(2);

		  	var inv = { 
		  	 amount_due: 'Amount Paid: R'+amt_due, 
		  	 amount_paid: 'Amount Owe: R'+amt_paid, 
		  	 desc: r.invoices[i].invoice_status_desc,
		  	 issue_date: 'Date Issued: '+r.invoices[i].issue_date
		  	  }; 
		  	InvList.push(inv); 
		
	  }}, function (e) {
	    console.log(e);
	});

 pageData.set("InvList", InvList);
};
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.logout = function() {
  frameModule.topmost().navigate("login/login");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
}
function onNavBtnTap() { 
   frameModule.topmost().navigate("main/main")
}
exports.recharge = function() {
  frameModule.topmost().navigate("recharge/recharge");
};
exports.customer = function() {
  frameModule.topmost().navigate("customers/customers");
};
exports.buy = function() {
  frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
exports.sell = function() {
  frameModule.topmost().navigate("sellVoucher/sellVoucher");
};

exports.loaded = loaded;
exports.onNavBtnTap = onNavBtnTap;

