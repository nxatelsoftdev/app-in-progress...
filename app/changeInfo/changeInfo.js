//Declaration Block.
var cust_info;
var session_id;
var i_cust;
var http = require("http");
var Observable = require("data/observable").Observable;
var observableArray = require("data/observable-array");
var observableModule = require("data/observable");
var appSettings = require("application-settings");
var frameModule = require("ui/frame");
var page;
var dialogsModule = require("ui/dialogs");
var drawer;
var pageData = new Observable({
	 
});
exports.home = function(){
	frameModule.topmost().navigate("main/main");
};
exports.sell = function(){
	frameModule.topmost().navigate("sellVoucher/sellVoucher");
};
exports.buy = function(){
	frameModule.topmost().navigate("buyVoucher/buyVoucher");
};
exports.customer = function(){
	frameModule.topmost().navigate("customers/customers");
};
exports.loaded = function(args){
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	i_cust = cust_info.i_customer;
	var pass1= cust_info.password;
	var tell1= cust_info.phone1;
	var email1= cust_info.email;
	var address1= cust_info.baddr1;
	//=====second gridview data=======
	var fax = cust_info.faxnum;
	var fname = cust_info.firstname;
	var lname = cust_info.lastname;
	var username = cust_info.login;
	var city = cust_info.city;
	var province = cust_info.state;
	var address_2 = cust_info.baddr2;
	var country = cust_info.country;
	var comp_name = cust_info.companyname;
	var refnum = cust_info.refnum;

	pageData.set('password', pass1);
	pageData.set('email', email1);
	pageData.set('tel', tell1);
	pageData.set('address', address1);
	//=====second gridview setting data========
	pageData.set('fax', fax);
	pageData.set('fname',fname);
	pageData.set('lname',lname);
	pageData.set('username', username);
	pageData.set('city', city);
	pageData.set('province', province);
	pageData.set('country', country);
	pageData.set('address2', address_2);
	pageData.set('companyname', comp_name);
	pageData.set('ref_num', refnum);

	page = args.object;
	drawer = page.getViewById("drawer");
	var menu = 0xe9ba;      
    pageData.set("menu", String.fromCharCode(menu));
    pageData.set("showLogin", true);
	page.bindingContext=pageData;

}

exports.save = function() {
	pageData.set("showLogin",false);
	session_id = JSON.parse(appSettings.getString("session_id"));
	cust_info = JSON.parse(appSettings.getString("customer_info"));
	var user_name = cust_info.login;
	var pass = pageData.get('password');
	var phone = pageData.get('tel');
	var email =pageData.get('email');
	var add = pageData.get('address');
	//=====second gridview getting data========
	var faxn = pageData.get('fax');
	var fname = pageData.get('fname');
	var lname = pageData.get('lname');
	var username=pageData.get('username');
	var country = pageData.get('country');
	var add2 = pageData.get('address2');
	var province = pageData.get('province');
	var city = pageData.get('city');
	var companyname = pageData.get('companyname');
	var refe_num = pageData.get('ref_num');
   
	var encoded = encodeURI('https://web.vodcs.co.za:443/rest/Customer/update_customer/{"session_id":'+session_id+'}/{"customer_info":{"i_customer":"'+i_cust+'","companyname":"'+companyname+'", "login":"'+username+'","Country":"'+country+'","state":"'+province+'","city":"'+city+'","baddr2":"'+add2+'","firstname":"'+fname+'","lastname":"'+lname+'","phone1":"'+phone+'","i_customer_class": "335","password":"'+pass+'","refnum":"'+refe_num+'","name":"'+companyname+'","faxnum":"'+faxn+'","email":"'+email+'","baddr1":"'+add+'"}}');
 
	http.getJSON(encoded).then(function (response) {
		var encoded= encodeURI('https://web.vodcs.co.za:443/rest/Customer/get_customer_info/{"session_id": '+session_id+'}/{"login": "'+username+'"}');
			http.getJSON(encoded).then(function (response) {
				appSettings.setString("customer_info", JSON.stringify(response.customer_info));
				pageData.set("showLogin",true);
				dialogsModule.alert({
				title: "Change Info",
				message: "Changes Succesfully Made.",
				okButtonText: "Close"
				}).then(function () {
				    console.log("Dialog closed!");
				});
			})
		
		frameModule.topmost().navigate("main/main");
		}),function(e){
		console.log(e);
	}
	
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.signIn = function() {
   frameModule.topmost().navigate("main/main");
};
exports.logout = function() {
   frameModule.topmost().navigate("login/login");
};
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Main Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "YES",
  		cancelButtonText: "NO",
  		
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};

//takes you to a previous page
function onNavBtnTap() {  
   frameModule.topmost().navigate("main/main")
}
exports.onNavBtnTap = onNavBtnTap;
