//Declaration block
var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var http = require("http");
var page;
var appSettings = require("application-settings");
var dialogsModule = require("ui/dialogs");
var view = require("ui/core/view");
var amount;
var ZXing = require("nativescript-zxing");
var v_id;
var bill_st;
var cust;
var i_acc;
var cust_info;
var cust_type;
var i_cust;
var drawer;
var ImageModule = require("ui/image");
//var img = new ImageModule.Image();
//img.src = "http://spimeproject.com/wp-content/uploads/2010/07/spimebox_com_20.png";
var session_id = JSON.parse(appSettings.getString("session_id"));
//binds the xml textfields with the values on the js		
var pageData = new Observable();

function loaded(args){
	var cust;
    var page = args.object;
	cust = page.navigationContext;
	//v_id = voucher.id;
	console.dump(cust);
	//storing the bill_staus of a selected voucher.
	//bill_st = voucher.bill_status;
	//i_acc = voucher.i_account;
	//curBal = parseFloat(voucher.balance);
	//curBal = curBal.toFixed(2);
	//pageData.set('amount',"Amount: R "+curBal);
	//pageData.set('voucherNumber',"Voucher No: "+voucher.id);
	//letting a seller know that this voucher is not redeemed and button is written redeem.
	//if (bill_st == "O"){
	//	alert('Oops, This voucher has been sold, please choose another one.');
	//	frameModule.topmost().navigate("sellVoucher/sellVoucher");
	//}else{
	//	pageData.set('status',"NOT REDEEMED");
	//	pageData.set('redeem',"REDEEM NOW");
	//}

	var menu = 0xe9ba;      
        pageData.set("menu", String.fromCharCode(menu));
    drawer = page.getViewById("drawer");
	page.bindingContext = pageData;
}
exports.toggleDrawer = function() {
  drawer.toggleDrawerState();
};
exports.home = function(){
	frameModule.topmost().navigate("main/main");
};
exports.customer = function(){
	frameModule.topmost().navigate("customers/customers");
};
function onNavBtnTap() {  
   frameModule.topmost().navigate("sellVoucher/sellVoucher")
}
exports.account = function() {
  frameModule.topmost().navigate("account/account");
};
function logout() {  
   frameModule.topmost().navigate("login/login")
}
exports.rechargeaccount = function(){
	frameModule.topmost().navigate("recharge/recharge");
};
exports.change = function(){
	frameModule.topmost().navigate("changeInfo/changeInfo");
};
exports.addcust = function(){
	frameModule.topmost().navigate("addcust/addcust");
};
exports.exit = function() {
	dialogsModule.confirm({
		title: "Scan Voucher Page",
  		message: "Are You Sure You Want To Exit?",
  		okButtonText: "Yes",
  		cancelButtonText: "No"
	}).then(function (result) {
		if(result == true){
			android.os.Process.killProcess(android.os.Process.myPid());	
		}
});
};
exports.onNavBtnTap = onNavBtnTap;
exports.loaded =loaded;
exports.logout = logout;

